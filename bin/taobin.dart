import 'dart:io';

class Taobin {
  var _name;
  var _price;
  var _sweetness;
  bool straw = false;
  bool lid = false;

  getName() { 
    return _name; 
    }
  getPrice() {
     return _price; 
     }

  setName(name) {
     _name = name; 
     }
  setPrice(price) {
     _price = price; 
     }
  setSweet(sweet) {
     _sweetness = sweet; 
     }
  setStraw(b) {
    if(b){straw=true;}
    }
  setLid(b) {
    if(b){lid=true;}
    }

  Taobin(name, price) {
    _name = name;
    _price = price;
  }

  text(n) {
    return "${n}.${_name} ${_price} บาท";
  }

  summary() {
    return "\n${_name} ${_sweetness} ${_price} บาท";
  }
}

class DrinkType {
  var _name;
  List<Taobin> _drinkList = [];

  getName() {
    return _name;
    }
  getDrinks() {
    return _drinkList;
    }

  setName(name) {
     _name = name; 
     }
  setDrinks(drinks) {
     _drinkList = drinks; 
     }

  DrinkType(String name,{taobin:0}) {
    _name = name;
    if (taobin != 0) {
      _drinkList = taobin;
    }
  }  

  addDrink(name, price) {
    _drinkList.add(new Taobin(name, price));
  }

  showDrinks() {
    print("\n${_name}");
    for (int i=0; i<_drinkList.length; i++) {
      print(_drinkList[i].text(i+1));
    }
  }

  getSelectedDrink(index) {
    return _drinkList[index];
  }
} 

void main() {

  DrinkType recommended = new DrinkType("Hot Menu!!");
  DrinkType coffee = new DrinkType("Coffee");
  DrinkType tea = new DrinkType("Tea");
  DrinkType milk = new DrinkType("Milk");
  
  recommended.addDrink('Americano',50);
  recommended.addDrink('Matcha latte',55);
  recommended.addDrink('Brown sugar milk',50);

  coffee.addDrink('Americano',50);
  coffee.addDrink('Espresso',45);
  coffee.addDrink('Cappuchino',40);
  coffee.addDrink('Latte',40);
  
  tea.addDrink('Thai tea',35);
  tea.addDrink('Green tea',35);
  tea.addDrink('Taiwan tea',35);
  tea.addDrink('Black tea',40);

  milk.addDrink('Fresh milk',35);
  milk.addDrink('Strawberry milk',50);
  milk.addDrink('Almond milk',50);
  milk.addDrink('Pink milk',35);

  List<DrinkType> menu = [recommended,coffee,tea,milk];
  var sweet = ['No sweet','Less sweet','Normal sweet','More sweet'];
  var pay_method;
  var curr_drink;

  print('Welcome to Taobin\nPlease select Menu Type');
  for (int i=0; i<menu.length; i++) {
    print('${i+1}.${menu[i].getName()}');
  }

  print("\nMenu Type : ");
  int? selected_type = int.parse(stdin.readLineSync()!)-1;
  menu[selected_type].showDrinks();

  print("\nMenu : ");
  int? selected_drink = int.parse(stdin.readLineSync()!)-1;
  curr_drink = menu[selected_type].getSelectedDrink(selected_drink);

  print("\nSweet level");
  int i = 1;
  sweet.forEach((s) => print('${i++}.${s}'));
  print("\nSweet : ");
  int? selectedSweet = int.parse(stdin.readLineSync()!)-1;
  curr_drink.setSweet(sweet[selectedSweet]);

  print(curr_drink.summary());
  
  print("\nเลือกวิธีการชำระเงิน\nชำระเป็นเงินสด/ชำระโดยการโอน");
  print("\nชำระเงิน : ");
  String? payment = stdin.readLineSync()!;
  pay_method = payment;
  print("\nThank you!!!");
}
